# britecore-frontend

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Check it Live
http://britecore-test-frontend.s3-website-us-east-1.amazonaws.com

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
