import Vue from 'vue'
import App from './App.vue'
import router from './router'

import 'materialize-css'
import 'materialize-css/dist/css/materialize.css'
import M from 'materialize-css/dist/js/materialize';


Vue.config.productionTip = false

Vue.mixin({
  data: function() {
    return {
      backendHost: 'https://zqsjvxasl1.execute-api.us-west-2.amazonaws.com/dev',
    }
  }
})

new Vue({
  router,
  render: h => {
    setTimeout(() => {
      M.Sidenav.init(document.querySelectorAll('.sidenav'));
    }, 400)
    return h(App);
  }
}).$mount('#vue-app')
