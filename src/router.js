import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import CreateRisk from './views/CreateRisk'
import EditRisk from './views/EditRisk'
import AddData from './views/AddData'
import ViewData from './views/ViewData'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/create-risk',
      name: 'create-risk',
      component: CreateRisk,
    },
    {
      path: '/edit-risk/:id',
      name: 'edit-risk',
      component: EditRisk,
    },
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/add-data',
      name: 'add-data',
      component: AddData,
    },
    {
      path: '/view-data',
      name: 'view-data',
      component: ViewData,
    },
  ]
})
